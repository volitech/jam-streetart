class Player {
    constructor(id, username, points = 0) {
        this.id         = id;
        this.username   = username;
        this.points     = parseInt(points);
        this.drawing    = [];
        this.vote       = null;
    }

    setRandomVote(players) {
        var others = players.slice();

        others.splice(others.indexOf(this), 1);
        console.log(`Player #${this.id}, others:`, others);
        this.vote = others[parseInt(Math.random() * others.length)];
    }
}

module.exports = Player;
