const Vector2   = require("./Vector2");
const Player    = require("./Player");
const Utils     = require("./Utils");

const MIN_USERS_TO_START = 3;

const DURATION = {
    START:  10 * 1e3,
    GAME:   60 * 1e3,
    VOTE:   30 * 1e3
}

const STATE = {
    STANDBY:    0,
    PLAYING:    1,
    VOTING:     2
}
const IMAGES_COUNT = 8;

class EpiArt {
    constructor() {
        this.clients            = {};
        this.state              = STATE.STANDBY;
        this.lastId             = 1;
        this.imageId            = 0;
        this.shuffledPlayers    = [];
        this.startTimeout       = null;
        this.voteTimeout        = null;
        this.endTimeout         = null;
    }

    get clientsCount() { return Object.keys(this.clients).length; }
    get players() { return Object.values(this.clients).map(client => (client.player)); }

    addClient(socket, username) {
        var player = new Player(this.lastId++, username);

        this.broadcast(
            player,
            null,
            "join",
            [1, [[
                player.id,
                player.username,
                player.points
            ]]]
        );
        socket.emit("init", player.id);
        socket.emit("join", [0, Object.values(this.clients).map(
            client => ([
                client.player.id,
                client.player.username,
                client.player.points
            ])
        )]);
        this.clients[player.id] = socket;
        socket.player = player;
        player.socket = socket;
        console.log(`User #${player.id} joined`);
        if (this.clientsCount >= MIN_USERS_TO_START && !this.startTimeout)
            this.scheduleStart();
    }

    removeClient(socket) {
        this.broadcast(socket.player, null, "leave", [socket.player.id]);
        delete this.clients[socket.player.id];
        console.log(`User #${socket.player.id} left`);
        if (this.clientsCount < MIN_USERS_TO_START)
            this.abortGame();
    }

    scheduleStart() {
        this.broadcast(null, null, "schedule", Date.now() + DURATION.START);
        this.startTimeout = setTimeout(() => { this.start(); }, DURATION.START);
    }

    abortGame() {
        if (this.startTimeout || this.voteTimeout || this.endTimeout)
            this.broadcast(null, null, "abort");
        this.resetTimeouts();
        console.log("Round aborted");
    }

    resetTimeouts() {
        if (this.startTimeout) {
            clearTimeout(this.startTimeout);
            this.startTimeout = null;
        }
        if (this.voteTimeout) {
            clearTimeout(this.voteTimeout);
            this.voteTimeout = null;
        }
        if (this.endTimeout) {
            clearTimeout(this.endTimeout);
            this.endTimeout = null;
        }
    }

    start() {
        console.log("Starting round");
        this.state = STATE.PLAYING;
        this.resetTimeouts();
        this.imageId = parseInt(Math.random() * IMAGES_COUNT);
        this.players.forEach(player => {
            player.drawing = [];
            player.vote = null;
        });
        this.voteTimeout = setTimeout(() => {
            this.vote();
        }, DURATION.GAME + 3e3);
        this.finished = 0;
        this.broadcast(null, null, "start", [Date.now() + DURATION.GAME, this.imageId]);
    }

    submitDrawing(player, drawing) {
        if (!player.drawing.length) {
            player.drawing = drawing;
            if (++this.finished >= this.players.length)
                this.vote();
        }
    }

    vote() {
        this.shuffledPlayers = Utils.shuffle(this.players);

        var drawings = this.shuffledPlayers.map( player => (player.drawing));

        console.log("Starting vote");
        console.log("drawings:", drawings);
        console.log("this.shuffledPlayers:", this.shuffledPlayers);
        this.state = STATE.VOTING;
        this.resetTimeouts();
        this.voleTimeout = null;
        this.players.forEach(player => {
            player.socket.emit("vote", [
                Date.now() + DURATION.VOTE,
                this.shuffledPlayers.indexOf(player),
                drawings
            ]);
        });
        this.finished = 0;
        this.endTimeout = setTimeout(() => { this.finish(); }, DURATION.VOTE);
    }

    handleVote(player, drawing_idx) {
        if (!player.vote && this.shuffledPlayers[drawing_idx] != player) {
            player.vote = this.shuffledPlayers[drawing_idx];
            if (++this.finished >= this.players.length)
                this.finish();
        }
    }

    finish() {
        console.log("Finishing game");
        this.resetTimeouts();
        this.players.forEach(player => {
            if (!player.vote)
                player.setRandomVote(this.shuffledPlayers);
            if (this.clients[player.vote.id])
                this.clients[player.vote.id].player.points += 1;
        });
        this.broadcast(null, null, "results",
            this.players.map(player => ([player.id, player.points]))
        );
        this.state = STATE.STANDBY;
        if (this.clientsCount >= MIN_USERS_TO_START)
            this.scheduleStart();
    }

    sendMessage(sender, msg) {
        if (this.state == STATE.VOTING)
            return;
        this.broadcast(sender, null, "msg", [sender.id, msg], false);
    }

    broadcast(player, condition, event, data, exclude_self = true) {
        var other_player;

        for (var client of Object.values(this.clients)) {
            other_player = client.player;
            if (
                (!player || other_player.id != player.id || !exclude_self)
                && (!condition || condition(other_player))
            )
                other_player.socket.emit(event, data);
        }
    }
}

module.exports = EpiArt;
