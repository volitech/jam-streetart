class Vector2 {
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }

    clone() { return new Vector2(this.x, this.y) }

    getDist() { return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2)); }

    add(v) {
        if (v instanceof Vector2) {
            this.x += v.x;
            this.y += v.y;
            return this;
        }
        this.x += v;
        this.y += v;
        return this;
    }

    sub(v) {
        if (v instanceof Vector2) {
            this.x -= v.x;
            this.y -= v.y;
            return this;
        }
        this.x -= v;
        this.y -= v;
        return this;
    }

    mult(v) {
        if (v instanceof Vector2) {
            this.x *= v.x;
            this.y *= v.y;
            return this;
        }
        this.x *= v;
        this.y *= v;
        return this;
    }

    clampAbs(val) {
        this.x = Math.min(Math.max(this.x, -val), val);
        this.y = Math.min(Math.max(this.y, -val), val);
    }

    serialize() { return [this.x, this.y]; }
}

module.exports = Vector2;
