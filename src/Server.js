const fs        = require("fs");
const path      = require("path");
const express   = require("express");
const http      = require("http");
const io        = require("socket.io");

const EpiArt    = require("./EpiArt.js");

class Server {
    constructor(port = 8080) {
        this.app    = express();
        this.server = http.createServer(this.app);
        this.ws     = io(this.server);
        this.game   = new EpiArt();

        this.app.use((req, res) => {
            const file_path = path.join("views", req.path);

            if (req.path == "/")
                return res.status(200).sendFile(path.resolve("views/index.html"));
            if (!fs.existsSync(file_path))
                return res.status(404).sendFile(path.resolve("views/404.html"));
            res.status(200).sendFile(path.resolve(file_path));
        });
        this.ws.on("connection", socket => {
            socket.on("init", (username, skin_id) => {
                socket.isInit = true;
                this.game.addClient(socket, username, skin_id);
            });
            socket.on("draw", data => {
                if (!socket.isInit)
                    return;
                this.game.submitDrawing(socket.player, data);
            });
            socket.on("vote", drawing_idx => {
                if (!socket.isInit)
                    return;
                this.game.handleVote(socket.player, drawing_idx);
            });
            socket.on("msg", msg => {
                if (!socket.isInit)
                    return;
                this.game.sendMessage(socket.player, msg);
            });
            socket.on("disconnect", () => {
                if (socket.isInit)
                    this.game.removeClient(socket);
            });
        });
        this.server.listen(port, () => {
            console.log("EpiArt listening on port", port);
        });
    }
}

module.exports = Server;
