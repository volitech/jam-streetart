window.onload = () => {
    document.getElementById("play-button").addEventListener("click", start);
    document.addEventListener("keydown", e => {
        if (e.key == "Enter")
            start();
    });
}

function start() {
    var username = document.getElementById("player-name").value;

    if (!username)
        return alert("Please enter a username");
    window.localStorage.setItem("username", username);
    window.top.postMessage({
        username: username
    }, "*");
}
