class Painting {
    constructor(view, url, data, is_active = true) {
        this.app = new PIXI.Application({
            width:              800,
            height:             600,
            backgroundColor:    0xcccccc,
            view:               view,
            resolution:         1,
            forceCanvas:        true
        });
        this.stage          = this.app.stage;
        this.brushWidth     = 2;
        this.brushColor     = 0;
        this.isMouseDown    = false;
        this.canvas         = new Canvas(url, is_active);

        this.canvas.loadDrawing(url, data);
        this.stage.addChild(this.canvas.bg);
        this.stage.addChild(this.canvas.fg);
    }

    dispose() {
        console.log("DISPOSING");
        this.app.stop();
        this.app.renderer.destroy(true);
    }
}
