var game;

class Game {
    constructor(username) {
        var view = document.getElementById("game-canvas");

        this.app = new PIXI.Application({
            width:              800,
            height:             700,
            backgroundColor:    0xcccccc,
            view:               view,
            resolution:         2,
            resizeTo:           view.parentElement,
            forceCanvas:        true
        });
        this.socket             = io();
        this.stage              = this.app.stage;
        this.pressedKeys        = {};
        this.players            = {};
        this.isMouseDown        = false;
        this.player             = new Player(0, username);
        this.paintings          = [];
        this.selectedPainting   = -1;
        this.imageId            = -1;
        this.timer              = {start: 0, target: 0, label: "", is_infinite: true};
        this.canvas             = new Canvas("img/default_canvas.png");
        this.votePaintings      = [];

        this.setCanvas("img/default_canvas.png");
        this.addPlayer(this.player);
        this.initSocket(username);
        this.setTimer(0, "Waiting for players", true);
        this.app.ticker.add((deltaTime) => { this.update(this.app.ticker.elapsedMS); });
        document.addEventListener("keydown", e => { this.onKeyDown(e.key); });
        this.stage.addChild(this.canvas.bg);
        this.stage.addChild(this.canvas.fg);
        this.setMenu();
    }

    setCanvas(url) {
        this.canvas.clear();
        this.canvas.setBg(url);
    }

    setMenu() {
        var menu_graphics = new PIXI.Graphics();

        menu_graphics.beginFill(0xdddddd);
        menu_graphics.lineStyle(0, 0x000000);
        menu_graphics.drawRect(0, 600, 800, 640);
        this.stage.addChild(menu_graphics);
        this.setMenuButtons();
    }

    setMenuButtons() {
        var self = this;

        for (var i = 0; i < BRUSH_SIZES.length; i++) {
            this.addButton(
                `img/buttons/brush_${i}.png`,
                {x: 20 + 32 * i, y: 620},
                function(e) { self.canvas.brushSize = BRUSH_SIZES[this.brush_id]; }
            ).brush_id = i;
        }
        for (var i = 0; i < COLORS.length; i++) {
            this.addButton(
                `img/buttons/color_${i}.png`,
                {x: 200 + 32 * i, y: 620},
                function(e) { self.canvas.brushColor = this.brush_color; }
            ).brush_color = i;
        }
    }

    addButton(url, pos, callback) {
        var button  = new PIXI.Sprite(PIXI.Texture.from(url));
        var over_cb = function() { this.scale.set(1.1); };
        var out_cb  = function() { this.scale.set(1); };

        button.anchor.set(0.5);
        button.position.set(pos.x, pos.y);
        button.buttonMode   = true;
        button.interactive  = true;
        button
            .on("mousedown", callback)
            .on("mousedown", out_cb)
            .on("touchstart", over_cb)
            .on("mouseup", over_cb)
            .on("touchend", out_cb)
            .on("mouseupoutside", out_cb)
            .on("touchendoutside", out_cb)
            .on("mouseover", over_cb)
            .on("mouseout", out_cb)
        this.stage.addChild(button);
        return button;
    }

    initSocket(username) {
        this.socket.emit("init", username);
        this.socket.on("init", my_id => {
            console.log(`Player #${my_id} here!`);
            this.player.id = my_id;
            delete this.players[0];
            this.players[my_id] = this.player;
        });
        this.socket.on("join", join_data => {
            for (var data of join_data[1]) {
                if (join_data[0])
                    this.log(`${data[1]} joined`);
                this.addPlayer(new Player(data[0], data[1], data[2]));
            }
        });
        this.socket.on("leave", id => {
            this.log(`${this.players[id].username} left`);
            this.players[id].dispose();
            delete this.players[id];
        });
        this.socket.on("msg", data => {
            this.logMsg(this.players[data[0]], data[1])
        });
        this.socket.on("schedule", start_time => {
            this.setTimer(start_time, "Starting");
            this.log(`Starting round in ${Math.round((start_time - Date.now()) / 1e3)}s.`);
        });
        this.socket.on("abort", start_time => {
            this.setTimer(0, "Waiting for players", true);
            this.log(`Not enough players left. Round aborted.`);
            this.reset();
        });
        this.socket.on("start", data => {
            let [end_time, image_id] = data;

            this.setTimer(end_time, "Drawing");
            this.log(`Round started.`);
            this.imageId = image_id;
            this.setCanvas(`img/streets/${image_id}.jpg`);
            setTimeout(() => {
                this.socket.emit("draw", this.canvas.paths);
            }, end_time - Date.now());
        });
        this.socket.on("vote", data => {
            let [end_time, self_idx, paintings] = data;

            this.setTimer(end_time, "Voting");
            this.log(`Vote started.`);
            console.log("Voting for:", paintings);
            this.votePaintings = window.triggerVote(this.imageId, paintings, self_idx);
        });
        this.socket.on("results", player_points => {
            this.setTimer(0, "Waiting for players");
            this.log(`Round results:`);
            player_points.forEach(data => {
                let [player_id, points] = data;

                this.log(this.players[player_id].username + ": +" +
                    (points - this.players[player_id].points));
                this.players[player_id].setPoints(points);
            })
            this.reset();
        });
        this.socket.on("disconnect", () => {
            alert("Connection lost.");
        });
    }

    addPlayer(player) {
        if (this.players[player.id])
            return console.error("Error: duplicate player", player.id);
        this.players[player.id] = player;
        // this.stage.addChild(player.container);
    }

    onKeyDown(key) {
        if (key == "z")
            return this.canvas.undo();
        if (key == "Enter") {
            let input = document.getElementById("msg-input");

            this.sendMsg(input.value);
            input.value = "";
            return;
        }
    }

    log(msg) {
        var el = document.createElement("p");
        var chat_content = document.getElementById("chat-content");

        el.innerText = msg;
        el.classList.add("log");
        chat_content.appendChild(el);
        chat_content.scrollTo(0, chat_content.scrollHeight);
    }

    logMsg(player, msg) {
        var el = document.createElement("p");
        var span = document.createElement("span");

        span.innerText = player.username + ": ";
        el.appendChild(span);
        el.innerText += msg;
        el.classList.add("msg");
        document.getElementById("chat-content").appendChild(el);
    }

    sendMsg(msg) {
        if (!msg)
            return;
        this.socket.emit("msg", msg);
    }

    vote(vote_idx) {
        console.log("Voted for #", vote_idx);
        this.socket.emit("vote", vote_idx);
        this.reset();
    }

    reset() {
        this.setCanvas("img/default_canvas.png");
        this.votePaintings.forEach(painting => { painting.dispose(); });
        this.votePaintings = [];
        window.hideVote();
    }

    setTimer(target_time, label = "Standby", is_infinite = false) {
        this.timer.start        = Date.now();
        this.timer.target       = target_time;
        this.timer.label        = label;
        this.timer.is_infinite  = is_infinite;
        this.update();
    }

    updateTimer() {
        var time    = this.timer.target - Date.now();
        var ratio   = this.timer.is_infinite
            ? 0
            : 1 - time / (this.timer.target - this.timer.start);
        var timer   = document.getElementById("timer");

        if (ratio < 0 || ratio > 1) {
            timer.children[0].innerText = `${this.timer.label} - Complete`;
            return;
        }
        timer.children[0].innerText = this.timer.is_infinite
            ? this.timer.label
            : `${this.timer.label}: ${parseInt(time / 1e3 + 1)}s`;
        timer.children[2].style.width = `${ratio * 300}px`;
    }

    update(dt) {
        this.updateTimer();
    }
}

PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.LINEAR;

window.onload = () => {
    var vote_btn = document.getElementById("vote-btn");

    window.onmessage = e => {
        var menu_el = document.getElementById("menu-div");

        if (menu_el.classList[0])
            return;
        menu_el.classList.add("hidden");

        game = new Game(e.data.username);
    };

    vote_btn.addEventListener("click", e => {
        game.vote(game.selectedPainting);
        vote_btn.disabled = true;
    });
}

function scrollTo(element, to, duration) {
    var start       = element.scrollLeft;
    var change      = to - start;
    var time        = 0;
    var dt          = 30;
    var step        = () => {
        time += dt;
        element.scrollLeft = Math.easeInOutQuad(time, start, change, duration);
        if (time < duration)
            setTimeout(step, dt);
    };
    step();
}

function triggerVote(street_id, paintings, self_idx) {
    var holder      = document.getElementById("paintings");
    var controls    = document.getElementById("paintings-controls");
    var vote_btn    = document.getElementById("vote-btn");
    var i           = 0;
    var ret         = [];

    holder.innerHTML        = "";
    controls.innerHTML      = "";
    vote_btn.style.display  = "none";
    vote_btn.disabled       = false;
    document.getElementById("game-div").classList.add("flipped");
    for (let painting of paintings) {
        let parent = document.createElement("div");
        let canvas = document.createElement("canvas");
        let button = document.createElement("div");

        parent.classList.add("painting");
        button.setAttribute("painting-id", i);
        button.addEventListener("click", e => {
            let id = e.target.getAttribute("painting-id");
            let target = holder.children[id];

            document.querySelectorAll(".active").forEach(e => {
                e.classList.remove("active");
            });
            target.classList.add("active");
            e.target.classList.add("active");
            scrollTo(holder.parentElement, Math.min(
                (id - (id > self_idx)) * 410 - 180, 410 * (i - 3) + 50),
                200
            );
            vote_btn.style.display  = "inline-block";
            game.selectedPainting   = id;
        });
        parent.appendChild(canvas);
        holder.appendChild(parent);
        controls.appendChild(button);
        if (i == self_idx) {
            parent.style.display = "none";
            button.style.display = "none";
        }
        ret.push(
            new Painting(
                canvas,
                `img/streets/${street_id}.jpg`,
                painting,
                false
            )
        );
        i++;
    }
    return ret;
}

function hideVote() {
    document.getElementById("game-div").classList.remove("flipped");
}

Math.easeInOutQuad = (t, b, c, d) => {
    t /= d / 2;
    if (t < 1)
        return c / 2 * t * t + b;
    t--;
    return -c / 2 * (t * (t - 2) - 1) + b;
};
