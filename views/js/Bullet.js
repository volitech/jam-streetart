const BULLET_LIFETIME   = 3e3;
const BULLET_SPEED      = 1;

class Bullet {
    constructor(id, skin_id, pos = new Vector2(), vel = new Vector2(), rot = 0) {
        this.id         = id;
        this.pos        = pos.clone();
        this.vel        = vel.clone().add(new Vector2(Math.sin(rot), -Math.cos(rot)));
        this.rot        = rot;
        this.skin_id    = skin_id;
        this.lifetime   = BULLET_LIFETIME;
        this.sprite     = PIXI.Sprite.from(`assets/players/${skin_id}/bullet.png`);

        this.sprite.width       = 16;
        this.sprite.height      = 50;
        this.sprite.rotation    = rot;
        this.sprite.anchor.set(0.5);
    }

    update(dt) {
        this.pos.add(this.vel.clone().mult(dt * BULLET_SPEED));
        this.sprite.position.set(this.pos.x, this.pos.y);
        this.lifetime -= dt;
        if (this.lifetime < 1e3)
            this.sprite.alpha = this.lifetime / 1e3;
        if (this.lifetime <= 0)
            this.sprite.destroy();
    }

    serialize() {
        return [
            this.id,
            this.pos.serialize(),
            this.vel.serialize(),
            Math.round(this.rot * 1e2) / 1e2,
            this.skin_id
        ];
    }

    unserialize(data) {
        if (
            data.length != 5 ||
            data[1].length != 2 ||
            data[2].length != 2
        )
            return false;
        this.id         = parseInt(data[0]);
        this.pos.x      = parseFloat(data[1][0]);
        this.pos.y      = parseFloat(data[1][1]);
        this.vel.x      = parseFloat(data[2][0]);
        this.vel.y      = parseFloat(data[2][1]);
        this.rot        = parseFloat(data[3]);
        this.skin_id    = parseFloat(data[4]);
        this.sprite.rotation = this.rot;
        return true;
    }
}
