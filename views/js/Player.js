class Player {
    constructor(id, username, points = 0) {
        this.id         = id;
        this.username   = username;
        this.points     = points;
        this.isMine     = !id;
        this.element    = document.createElement("div");

        var name_el     = document.createElement("p");
        var points_el   = document.createElement("span");

        name_el.innerText = username;
        points_el.innerText = points;
        this.element.appendChild(name_el);
        this.element.appendChild(points_el);
        document.getElementById("players").appendChild(this.element);
    }

    setPoints(points) {
        this.points = points;
        this.element.children[1].innerText = points;
    }

    dispose() {
        document.getElementById("players").removeChild(this.element);
    }
}
