class Vector2 {
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }

    set(x = 0, y = 0) {
        this.x = x;
        this.y = y;
        return this;
    }

    clone() { return new Vector2(this.x, this.y) }

    getDist() { return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2)); }

    getDistToPoint(x, y) { return Math.sqrt(Math.pow(this.x - x, 2) + Math.pow(this.y - y, 2)); }

    add(v) {
        if (v instanceof Vector2) {
            this.x += v.x;
            this.y += v.y;
            return this;
        }
        this.x += v;
        this.y += v;
        return this;
    }

    sub(v) {
        if (v instanceof Vector2) {
            this.x -= v.x;
            this.y -= v.y;
            return this;
        }
        this.x -= v;
        this.y -= v;
        return this;
    }

    mult(v) {
        if (v instanceof Vector2) {
            this.x *= v.x;
            this.y *= v.y;
            return this;
        }
        this.x *= v;
        this.y *= v;
        return this;
    }

    clampAbs(val) {
        this.x = Math.min(Math.max(this.x, -val), val);
        this.y = Math.min(Math.max(this.y, -val), val);
        return this;
    }

    serialize() {
        return [
            Math.round(this.x * 1e2) / 1e2,
            Math.round(this.y * 1e2) / 1e2
        ];
    }
}
