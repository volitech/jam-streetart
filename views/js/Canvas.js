const BRUSH_SIZES = [2, 8, 14, 20, 26];
const COLORS = [
    0x000000,
    0xffffff,
    0xff0000,
    0xffaa00,
    0xffff00,
    0x00cc00,
    0x00ccff,
    0x5555ff,
    0xff00ff
];

class Canvas {
    constructor(url, is_active = true, paths = []) {
        this.brushSize      = 2;
        this.brushColor     = 0;
        this.isActive       = is_active;
        this.isMouseDown    = false;
        this.brushPos       = new Vector2();
        this.prevBrushPos   = new Vector2();
        this.paths          = paths;
        this.lines          = [];
        this.bg             = new PIXI.Sprite(PIXI.Texture.from(url));
        this.fg             = new PIXI.Container();
        this.fg.hitArea     = new PIXI.Rectangle(0, 0, 800, 600);
        this.fg.interactive = true;

        this.fg.on("mousedown", e => { this.onMouseDown(e); });
        this.fg.on("mousemove", e => { this.onMouseMove(e); });
        this.fg.on("mouseup",   e => { this.isMouseDown = false; });
        this.setBg(url);
    }

    setBg(url) {
        this.bg.texture = PIXI.Texture.from(url);
    }

    onMouseDown(e) {
        if (!this.isActive)
            return;
        this.prevBrushPos.set(e.data.global.x, e.data.global.y);
        this.isMouseDown = true;
        this.addLine(true);
        this.paths.unshift([
            this.brushSize,
            this.brushColor,
            [[
                Math.round(this.prevBrushPos.x),
                Math.round(this.prevBrushPos.y)
            ]]
        ]);
    }

    onMouseMove(e) {
        const MIN_MOVE_DISTANCE = 4;
        var mouse_pos           = e.data.global;

        if (
            !this.isActive
            || !this.isMouseDown
            || this.prevBrushPos.getDistToPoint(mouse_pos.x, mouse_pos.y) <
                MIN_MOVE_DISTANCE
        )
            return;
        this.brushPos.set(mouse_pos.x, mouse_pos.y);
        this.drawLine();
        this.paths[0][2].push([
            Math.round(mouse_pos.x),
            Math.round(mouse_pos.y)
        ]);
    }

    addLine(is_new) {
        if (is_new)
            this.lines.unshift([]);
        this.lines[0].unshift(new PIXI.Graphics());
        this.fg.addChild(this.lines[0][0]);
    }

    drawLine(
        brush_size  = this.brushSize,
        brush_color = this.brushColor
    ) {
        const STEP          = 1;
        const BUFFER_SIZE   = 1e6;
        var delta           = this.brushPos.clone().sub(this.prevBrushPos);
        var angle           = Math.atan2(delta.x, delta.y);
        var dir             = new Vector2(
                                Math.sin(angle),
                                Math.cos(angle)
                            ).mult(STEP);


        if (!this.lines[0])
            return;
        this.lines[0][0].beginFill(COLORS[brush_color]);
        while (
            this.prevBrushPos.getDistToPoint(
                this.brushPos.x,
                this.brushPos.y
            ) > STEP
        ) {
            this.lines[0][0].drawCircle(
                this.prevBrushPos.x,
                this.prevBrushPos.y,
                brush_size
            );
            this.prevBrushPos.add(dir);
        }
        if (this.lines[0][0].geometry._buffer.data.byteLength > BUFFER_SIZE)
            this.addLine(false);
        this.prevBrushPos.set(this.brushPos.x, this.brushPos.y);
    }

    loadDrawing(url, data) {
        this.setBg(url);
        this.bg.removeChildren();

        this.clear();
        data.reverse();
        data.forEach(el => {
            let brush_size  = el[0];
            let brush_color = el[1];
            let points      = el[2];

            this.addLine(true);
            this.prevBrushPos.set(points[0][0], points[0][1]);
            points.slice(1).forEach(point => {
                this.brushPos.set(point[0], point[1]);
                this.drawLine(brush_size, brush_color);
            });
        });
    }

    undo() {
        if (this.isMouseDown || !this.lines.length)
            return;

        var lines = this.lines.shift();

        lines.forEach(line => { line.destroy(); });
        this.paths.shift();
    }

    clear() {
        this.lines.forEach(sublines => {
            sublines.forEach(line => { line.destroy(); });
        });
        this.lines = [];
        this.paths = [];
    }
}
