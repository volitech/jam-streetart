# ![EpiArt](./views/menu/images/title.png)
EpiArt is an online browser game. The goal of the game is to draw the best street art in a limited period of time.

# Installation
1. Clone this repository and navigate into it
1. Install dependencies by running `npm i`
1. Launch the server by running `npm start`
1. Done! You can now navigate to `http://127.0.0.1:8080/` in your browser and start playing.

If you wish to play on your local network, tell other players to open the same URL, replacing `127.0.0.1` with your local IP address (make sure to be on the same network though!)

**Note**: *If other players are unable to connect, check your firewall: you may want to disable it or whitelist port `8080`*

# Gameplay
Each game is divided into two phases:

## Drawing
During the drawing phase, all players are provided with a canvas with a street background to draw on. They can pick a brush size and a color to paint "street art" on the provided picture. This is the longest phase, lasting one minute.

## Voting
After the drawing phase, all players are prompted with a voting screen, randomly shuffling all other players' drawings (one cannot vote for themselves). Once everyone has voted, or after 30 seconds, the voting phase ends, players are awarded the amount of votes they spent and, after a short delay, a new round starts. If a player does not vote, their vote is picked randomly.

# Authors
@volifter\
@KuroAndShiro
